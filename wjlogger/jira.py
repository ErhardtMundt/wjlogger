from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from datetime import datetime
from os.path import basename, splitext, isfile
from itertools import tee


class Communicator:

  def __init__(self, username, password):
    self._username = username
    self._password = password

  def __enter__(self):
    self._driver = webdriver.Chrome('wjlogger/webdrivers/chromedriver')
    self._driver.implicitly_wait(30)
    self.login()
    return self

  def __exit__(self, exc_type, exc_value, traceback):
    self._driver.close()

  def login(self):
    self._driver.get('https://watticsagile.atlassian.net/')
    element_username = self._driver.find_element_by_id('username')
    element_username.send_keys(self._username)
    element_password = self._driver.find_element_by_id('password')
    element_password.send_keys(self._password)
    element_password.send_keys(Keys.ENTER)

  def log_work(self, log):
    self._driver.get('https://watticsagile.atlassian.net/browse/WD-%s' % log.ticket_code)
    element_stalker = self._driver.find_element_by_id('stalker')
    element_stalker.find_element_by_id('opsbar-operations_more').click()
    self._driver.find_element_by_link_text('Log work').click()

    time_spent = timedelta_string(log.time_spent)
    element_time_spent = self._driver.find_element_by_id('log-work-time-logged')
    element_time_spent.send_keys(Keys.CONTROL + 'a')
    element_time_spent.send_keys(time_spent)

    start_datetime = datetime_string(log.start_datetime)
    element_start_datetime = self._driver.find_element_by_id('log-work-date-logged-date-picker')
    element_start_datetime.send_keys(Keys.CONTROL + 'a')
    element_start_datetime.send_keys(start_datetime)

    self._driver.find_element_by_id('log-work-submit').click()
    self._driver.find_element_by_css_selector('div.success')


class WorkLog:

  def __init__(self, ticket_code, start_datetime, time_spent):
    self._ticket_code = ticket_code
    self._time_spent = time_spent
    self._start_datetime = start_datetime

  def __repr__(self):
    return '<WorkLog %s: %s (%s)>' % (
      self._ticket_code,
      datetime_string(self._start_datetime),
      timedelta_string(self._time_spent)
    )

  @property
  def ticket_code(self):
    return self._ticket_code

  @property
  def start_datetime(self):
    return self._start_datetime

  @property
  def time_spent(self):
    return self._time_spent

  def is_valid(self):
    is_ticket_code_a_valid_number = int(self._ticket_code) != 0
    if not is_ticket_code_a_valid_number:
      return False

    return True


class MalformedWorklogFile(Exception):

  def __init__(self, message=None):
    super(Exception, self).__init__(message)


class WorkLogfileParser:

  def __init__(self, filepath):
    self._filepath = filepath

  def date(self):
    """
    Gets the date the log file is for.
    """

    filename = basename(self._filepath)
    filename_without_extension, _ = splitext(filename)
    return datetime.strptime(filename_without_extension, '%Y%m%d').date()

  def parse(self):
    """
    Parses the log file and returns a collection of WorkLog instances.
    """

    if not isfile(self._filepath):
      return None

    with open(self._filepath) as logfile:
      all_logfile_tokens_lines = [line.strip().split() for line in logfile.readlines()]

    tickets_tokens_lines = [tokens_line for tokens_line in all_logfile_tokens_lines if len(tokens_line) == 2]

    last_ticket_code = tickets_tokens_lines[-1][1]
    if last_ticket_code != 'finish':
      raise MalformedWorklogFile('"finish" expected as last ticket code, got "%s"' % last_ticket_code)

    a, b = tee(tickets_tokens_lines)
    next(b, None)
    iterator = zip(a, b)

    log_date = self.date()

    def ticket_time_difference(log_struc):
      previous_log_struc, next_log_struc = log_struc
      datetime_from = datetime.strptime(previous_log_struc[0], '%H%M')
      datetime_to = datetime.strptime(next_log_struc[0], '%H%M')
      time_elapsed = datetime_to - datetime_from
      ticket_code = previous_log_struc[1]
      ticket_datetime = datetime.combine(log_date, datetime_from.time())
      return {
        'ticket_code': ticket_code,
        'start_datetime': ticket_datetime,
        'time_spent': time_elapsed
      }

    return [
      WorkLog(**entry)
      for entry in map(ticket_time_difference, iterator)
      if not entry['ticket_code'] == 'lunch'
    ]


def timedelta_string(log_timedelta):
  hours, remainder = divmod(log_timedelta.seconds, 3600)
  minutes = remainder//60

  time_string_tokens = []
  if hours > 0:
    time_string_tokens.append('%dh' % hours)
  if minutes > 0:
    time_string_tokens.append('%dm' % minutes)

  return ' '.join(time_string_tokens)


def datetime_string(log_datetime):
  return log_datetime.strftime('%d/%b/%y %-I:%M %p')
