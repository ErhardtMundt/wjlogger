import wjlogger.jira
from datetime import timedelta, datetime, date
import unittest


class TestWorkLogfileParser(unittest.TestCase):

  FIXTURES = {
    'valid_worklog_file': '20160806.log',
    'worklog_file_not_ending_with_finish_ticket': '20160807.log',
    'worklog_file_with_past_midnight_ticket_logs': '20160808.log'
  }

  def test_date(self):
    logfile_parser = wjlogger.jira.WorkLogfileParser('/foo/bar/20160917.log')
    log_date = logfile_parser.date()
    self.assertEqual(log_date.year, 2016)
    self.assertEqual(log_date.month, 9)
    self.assertEqual(log_date.day, 17)

  def test_parse_handles_non_existing_files(self):
    logfile_parser = wjlogger.jira.WorkLogfileParser('/foo/bar/20160917.log')
    try:
      logfile_parser.parse()
    except Exception:
      self.fail('Exception raised')

  def test_parse_returns_worklog_instances(self):
    worklog_filepath = 'wjlogger/test/%s' % self.FIXTURES['valid_worklog_file']
    logfile_parser = wjlogger.jira.WorkLogfileParser(worklog_filepath)
    parsed_item = logfile_parser.parse()[0]
    self.assertIsInstance(parsed_item, wjlogger.jira.WorkLog, 'Wrong instance type')

  def test_parse_skips_lunch_tickets(self):
    worklog_filepath = 'wjlogger/test/%s' % self.FIXTURES['valid_worklog_file']
    logfile_parser = wjlogger.jira.WorkLogfileParser(worklog_filepath)
    work_logs = logfile_parser.parse()
    for work_log in work_logs:
      self.assertNotEqual(work_log.ticket_code, 'lunch', 'Lunch worklog is created')

  def test_parse_raises_exception_if_finish_is_not_last_ticket(self):
    worklog_filepath = 'wjlogger/test/%s' % self.FIXTURES['worklog_file_not_ending_with_finish_ticket']
    logfile_parser = wjlogger.jira.WorkLogfileParser(worklog_filepath)
    with self.assertRaises(wjlogger.jira.MalformedWorklogFile):
      work_logs = logfile_parser.parse()

  def test_parse_correctly_calculate_past_midnight_workog_timedeltas(self):
    worklog_filepath = 'wjlogger/test/%s' % self.FIXTURES['worklog_file_with_past_midnight_ticket_logs']
    logfile_parser = wjlogger.jira.WorkLogfileParser(worklog_filepath)
    last_work_log = logfile_parser.parse()[-1]
    self.assertEqual(last_work_log.time_spent, timedelta(days=-1, hours=13), 'Unexpected time spent value')


class TestTimedeltaString(unittest.TestCase):

  def test(self):
    expected_matches = [
      [{'hours': 5}, '5h'],
      [{'hours': 7, 'minutes': 52}, '7h 52m']
    ]
    for match in expected_matches:
      time_spent = timedelta(**match[0])
      calculated_string = wjlogger.jira.timedelta_string(time_spent)
      expected_string = match[1]
      self.assertEqual(calculated_string, expected_string)


class TestDatetimeString(unittest.TestCase):

  def test(self):
    log_datetime = datetime(2013, 9, 30, 7, 6)
    calculated_string = wjlogger.jira.datetime_string(log_datetime)
    expected_string = '30/Sep/13 7:06 AM'
    self.assertEqual(calculated_string, expected_string)
