from configparser import ConfigParser
from codecs import decode, encode
from os.path import expanduser, isfile, join
from glob import glob
from shutil import move
import jira


def main():
  config_path = expanduser('~/.wjlogger')
  if not isfile(config_path):
    print('Config file not found')
    exit(-1)

  config = ConfigParser()
  config.read(config_path)

  username = config['Credentials']['username']
  password = decode(config['Credentials']['cnffjbeq'], 'rot13')
  source_folder = expanduser(config['Paths']['source'])
  archive_folder = expanduser(config['Paths']['archive'])

  worklog_filepaths = glob(join(source_folder, '*.log'))

  if len(worklog_filepaths) == 0:
    return

  with jira.Communicator(username, password) as j:
    for worklog_filepath in worklog_filepaths:
      parser = jira.WorkLogfileParser(worklog_filepath)
      try:
        for work_log in parser.parse():
          if not work_log.is_valid():
            print('Skipping %s' % work_log)
            continue
          print(work_log)
          j.log_work(work_log)
        move(worklog_filepath, archive_folder)
      except Exception as e:
        print('[ERROR] %s' % e)

if __name__ == '__main__':
  main()
